using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {
	
	
	bool gameHasEnded = false;

	public GameObject completeLevelUI;
	
	[SerializeField] Text finalscore;
	[SerializeField] GameObject EndTrigger;
	public GameObject Score;

	[SerializeField] GameOverManager gameOverManager;

	public void CompleteLevel ()
	{
		completeLevelUI.SetActive(true);	
	}

	public void EndGame ()
	{
		if (gameHasEnded == false)
		{
			gameHasEnded = true;
			Debug.Log("GAME OVER");
			gameOverManager.SetGameOver();
		
		}

	}

	void Restart ()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

}
