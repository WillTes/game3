using UnityEngine;

public class WheelCollision : MonoBehaviour
{

	public PlayerMovement movement;
	

	void OnCollisionEnter(Collision collisionInfo)
	{

		if (collisionInfo.collider.tag == "Obstacle")
		{
			movement.enabled = false;

			GetComponent<MeshRenderer>().enabled = false;
			GetComponent<BoxCollider>().enabled = false;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;

		}
	}
}
